﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;

namespace Confitec.Data
{
    /// <inheritdoc cref="IContext" />
    /// <summary>Provide context for using database handlers. </summary>
    [ExcludeFromCodeCoverage]
    public class ConfitecContext : DbContext, IContext
    {
        /// <summary>Initializes a new instance of the <see cref="ConfitecContext"></see></summary>
        /// <param name="options">Data context options.</param>
        public ConfitecContext(DbContextOptions<ConfitecContext> options)
            : base(options)
        { }


        /// <summary> Overrides OnModelCreating method. </summary>
        /// <param name="modelBuilder">ModelBuilder param. </param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(GetType().Assembly);
        }

        /// <inheritdoc />
        public DbSet<TEntity> CreateSet<TEntity>() where TEntity : class
        {
            try
            {
                return Set<TEntity>();
            }
            catch (Exception exception)
            {
                throw new DatabaseErrorException(exception.Message, exception);
            }
        }

        /// <inheritdoc />
        public int SaveContextChanges()
        {
            try
            {
                return SaveChanges();
            }
            catch (Exception exception)
            {
                throw new DatabaseErrorException(exception.Message, exception);
            }
        }

        /// <inheritdoc />
        public Task<int> SaveContextChangesAsync()
        {
            try
            {
                return SaveChangesAsync();
            }
            catch (Exception exception)
            {
                throw new DatabaseErrorException(exception.Message, exception);
            }
        }

        /// <inheritdoc />
        public void ChangeEntityState(object entity)
        {
            try
            {
                Entry(entity).State = EntityState.Modified;
            }
            catch (Exception exception)
            {
                throw new DatabaseErrorException(exception.Message, exception);
            }
        }
    }
}

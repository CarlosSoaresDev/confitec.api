﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Confitec.Data.Migrations
{
    public partial class AddTablePerson : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_PersonUser",
                table: "PersonUser");

            migrationBuilder.RenameTable(
                name: "PersonUser",
                newName: "Person");

            migrationBuilder.AlterColumn<int>(
                name: "Education",
                table: "Person",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int",
                oldUnicode: false,
                oldMaxLength: 1);

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "Person",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Person",
                table: "Person",
                column: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Person",
                table: "Person");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "Person");

            migrationBuilder.RenameTable(
                name: "Person",
                newName: "PersonUser");

            migrationBuilder.AlterColumn<int>(
                name: "Education",
                table: "PersonUser",
                type: "int",
                unicode: false,
                maxLength: 1,
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_PersonUser",
                table: "PersonUser",
                column: "Id");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Confitec.Data.Migrations
{
    public partial class AddTablePersonNatural : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Gender",
                table: "Person",
                maxLength: 1,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Gender",
                table: "Person");
        }
    }
}

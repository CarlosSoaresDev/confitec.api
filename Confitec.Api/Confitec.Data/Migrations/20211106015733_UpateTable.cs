﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Confitec.Data.Migrations
{
    public partial class UpateTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Password",
                table: "PersonUser",
                maxLength: 255,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Password",
                table: "PersonUser");
        }
    }
}

﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

namespace Confitec.Data
{
    /// <inheritdoc />
    /// <summary>Represents a Database Error Exception</summary>
    [ExcludeFromCodeCoverage]
    [Serializable]
    public class DatabaseErrorException : Exception
    {

        /// <inheritdoc />
        /// <summary>Initializes a new instance of the <see cref="DatabaseErrorException"></see></summary>
        public DatabaseErrorException() { }

        /// <inheritdoc />
        /// <summary>Initializes a new instance of the <see cref="DatabaseErrorException"></see></summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        public DatabaseErrorException(string message) : base(message) { }

        /// <inheritdoc />
        /// <summary>Initializes a new instance of the <see cref="DatabaseErrorException"></see></summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        /// <param name="innerException">The exception that is the cause of the current exception, or a null reference (Nothing in Visual Basic) if no inner exception is specified.</param>
        public DatabaseErrorException(string message, Exception innerException) : base(message, innerException) { }

        /// <inheritdoc />
        /// <summary>Initializes a new instance of the <see cref="DatabaseErrorException"></see> class with serialized data.</summary>
        /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo"></see> that holds the serialized object data about the exception being thrown.</param>
        /// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext"></see> that contains contextual information about the source or destination.</param>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="info">info</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Runtime.Serialization.SerializationException">The class name is null or <see cref="Exception.HResult"></see> is zero (0).</exception>
        protected DatabaseErrorException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Confitec.Data.PersonModule.PersonUserFeature;
using Confitec.Domain.Adapter;
using System.Linq;

namespace Confitec.Data.Adapter
{
    /// <summary>
    /// 
    /// </summary>
    public class AutoMapperObjectAdapter : IAutoMapperObjectAdapter
    {
        /// <summary>
        /// Fields for injection
        /// </summary>
        private readonly IMapper _mapper;
        private readonly MapperConfiguration _configuration;

        public AutoMapperObjectAdapter()
        {
            _configuration = Config();
            _mapper = _configuration.CreateMapper();
        }

        private MapperConfiguration Config()
        {
            var mapperConfiguration = new MapperConfiguration(mc =>
            {
                mc.AddProfile<PersonUserAdapter>();
            });

            return mapperConfiguration;
        }

        /// <inheritdoc/>
        public TTo Convert<TFrom, TTo>(TFrom source) where TFrom : class => _mapper.Map<TFrom, TTo>(source);

        /// <inheritdoc/>
        public TTo ConvertToTarget<TFrom, TTo>(TFrom source, TTo target) where TFrom : class => _mapper.Map<TFrom, TTo>(source, target);

        /// <inheritdoc/>
        public IQueryable<T> ProjectTo<T>(IQueryable source) => source.ProjectTo<T>(_configuration);

    }
}

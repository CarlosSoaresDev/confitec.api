﻿using Confitec.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Confitec.Data
{
    /// <inheritdoc />
    [ExcludeFromCodeCoverage]
    public abstract class Repository<TEntity> : IRepository<TEntity> where TEntity : EntityBase, new()
    {
        /// <summary>Data source context to data operations</summary>
        private readonly IContext _context;

        /// <inheritdoc />
        /// <summary> Get context data. </summary>
        /// <returns>Return a queryable of an entity. </returns>
        public IQueryable<TEntity> Data { get; internal set; }

        /// <summary>Initializes a new instance of the IRepository. </summary>
        /// <param name="context">Data source context to data operations</param>
        protected Repository(IContext context)
        {
            _context = context;
            Data = _context.CreateSet<TEntity>();
        }

        /// <inheritdoc />
        public TEntity FirstOrDefault(Expression<Func<TEntity, bool>> expression)
        {
            return Data.FirstOrDefault(expression);
        }

        /// <inheritdoc />
        public Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> expression, params Expression<Func<TEntity, object>>[] includes)
        {
            var localData = includes == null ? Data : includes.Aggregate(Data, (current, includeProperty) => current.Include(includeProperty));
            return localData.FirstOrDefaultAsync(expression);
        }

        /// <inheritdoc/>
        public Task SaveAsync(TEntity entity)
        {
            if (entity.Id > 0)
                _context.ChangeEntityState(entity);
            else
            {
                var dbSet = _context.CreateSet<TEntity>();
                dbSet.Add(entity);
            }
            return _context.SaveContextChangesAsync();
        }

        /// <inheritdoc/>
        public Task SaveAsync(IEnumerable<TEntity> entities)
        {
            foreach (var entity in entities)
            {
                if (entity.Id > 0)
                    _context.ChangeEntityState(entity);

                else
                {
                    var dbSet = _context.CreateSet<TEntity>();
                    dbSet.Add(entity);
                }
            }
            return _context.SaveContextChangesAsync();
        }

        /// <inheritdoc/>
        public void Save(TEntity entity)
        {
            if (entity.Id > 0)
                _context.ChangeEntityState(entity);
            else
            {
                var dbSet = _context.CreateSet<TEntity>();
                dbSet.Add(entity);
            }
            _context.SaveContextChanges();
        }

        /// <inheritdoc/>
        public void Save(IEnumerable<TEntity> entities)
        {
            foreach (var entity in entities)
            {
                if (entity.Id > 0)
                    _context.ChangeEntityState(entity);
                else
                {
                    var dbSet = _context.CreateSet<TEntity>();
                    dbSet.Add(entity);
                }
            }
            _context.SaveContextChanges();
        }

        /// <inheritdoc />
        public void Remove(TEntity entity)
        {
            var dbSet = _context.CreateSet<TEntity>();
            dbSet.Remove(entity);
            _context.SaveContextChanges();
        }

        /// <inheritdoc />
        public Task RemoveAsync(TEntity entity)
        {
            var dbSet = _context.CreateSet<TEntity>();
            dbSet.Remove(entity);

            return _context.SaveContextChangesAsync();
        }

        /// <inheritdoc />
        public void Remove(IEnumerable<TEntity> entities)
        {
            var dbSet = _context.CreateSet<TEntity>();

            foreach (var entity in entities)
                dbSet.Remove(entity);

            _context.SaveContextChanges();
        }

        /// <inheritdoc />
        public void RemoveAsync(IEnumerable<TEntity> entities)
        {
            var dbSet = _context.CreateSet<TEntity>();

            foreach (var entity in entities)
                dbSet.Remove(entity);

            _context.SaveContextChangesAsync();
        }

        /// <summary>Dispose current Data and reset all query inclusions.</summary>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                Data = _context.CreateSet<TEntity>();
            }
        }

        /// <inheritdoc />
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}


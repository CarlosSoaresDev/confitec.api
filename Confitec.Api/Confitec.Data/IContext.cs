﻿using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Confitec.Data
{
    /// <summary> Represents a contract of a system context. </summary>
    public interface IContext
    {
        /// <summary> Create set of a entity type. </summary>
        /// <typeparam name="T"> Represent a entity type to manipulate. </typeparam>
        /// <returns></returns>
        DbSet<T> CreateSet<T>() where T : class;

        /// <summary> Save the context changes in the BbSet. </summary>
        /// <returns> Return number of affected lines. </returns>
        int SaveContextChanges();

        /// <summary> Save the context changes in the BbSet. </summary>
        /// <returns> Return number of affected lines. </returns>
        Task<int> SaveContextChangesAsync();

        /// <summary> Set the entity state to Modified.  </summary>
        /// <param name="entity">Entity object. </param>
        void ChangeEntityState(object entity);
    }
}
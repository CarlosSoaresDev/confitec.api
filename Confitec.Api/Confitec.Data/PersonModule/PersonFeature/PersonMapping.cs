﻿using Confitec.Domain.PersonModule.PersonFeature;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.Diagnostics.CodeAnalysis;

namespace Confitec.Data.PersonModule.PersonFeature
{
    /// <summary> Represents the data table mapping "person". </summary>
    [ExcludeFromCodeCoverage]
    public class PersonMapping : IEntityTypeConfiguration<Person>
    {
        /// <summary> Execute the configuration mapping person user. </summary>
        /// <param name="builder">Current builder entity type.</param>
        public void Configure(EntityTypeBuilder<Person> builder)
        {
            builder.ToTable("Person");

            builder.HasKey(e => e.Id);

            builder.Property(e => e.Name).HasColumnName("Name").HasMaxLength(255).IsUnicode(false);
            builder.Property(e => e.NickName).HasColumnName("NickName").HasMaxLength(100).IsUnicode(false);
            builder.Property(e => e.BirthdayOrFoundationDate).HasColumnName("Birthday").HasColumnType("datetime");
        }
    }
}
﻿using AutoMapper;
using Confitec.Domain.PersonModule.PersonUserFeature;
using System.Diagnostics.CodeAnalysis;

namespace Confitec.Data.PersonModule.PersonUserFeature
{
    [ExcludeFromCodeCoverage]
    public class PersonUserAdapter: Profile
    {
        public PersonUserAdapter()
        {
            CreateMap<PersonUser, PersonUserDto>();

            CreateMap<PersonUserDto, PersonUser>();
        }
    }
}

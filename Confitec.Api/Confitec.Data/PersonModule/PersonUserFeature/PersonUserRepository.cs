﻿using Confitec.Domain.PersonModule.PersonUserFeature;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Confitec.Data.PersonModule.PersonUserFeature
{
    /// <inheritdoc/>
    [ExcludeFromCodeCoverage]
    public class PersonUserRepository : Repository<PersonUser>, IPersonUserRepository
    {
        private readonly ILogger<PersonUserRepository> _logger;

        /// <summary>Initializes a new instance of the <see cref="PersonUserRepository"></see></summary>
        /// <param name="context">Data source context to data operations</param>
        /// <param name="logger">Logger instance parameter</param>
        public PersonUserRepository(IContext context, ILogger<PersonUserRepository> logger)
            : base(context)
        {
            _logger = logger;
        }

        /// <inheritdoc/>
        public async Task CreateAsync(PersonUser personUser)
        {
            try
            {
                await SaveAsync(personUser);
            }
            catch (PersonUserException ex)
            {
                _logger.LogCritical(ex.Message, "PersonUserCommand >> CreateAsync");
                throw new PersonUserException(ex.Message);
            }
        }

        /// <inheritdoc/>
        public async Task UpdateAsync(PersonUser personUser)
        {
            try
            {
                await SaveAsync(personUser);
            }
            catch (PersonUserException ex)
            {
                _logger.LogCritical(ex.Message, "PersonUserCommand >> UpdateAsync");
                throw new PersonUserException(ex.Message);
            }
        }

        /// <inheritdoc/>
        public async Task DeleteAsync(int id)
        {
            try
            {
                var personUser = await FirstOrDefaultAsync(f => f.Id == id);

                await RemoveAsync(personUser);
            }
            catch (PersonUserException ex)
            {
                _logger.LogCritical(ex.Message, "PersonUserCommand >> DeleteAsync");
                throw new PersonUserException(ex.Message);
            }
        }

        public IQueryable<PersonUser> GetAllAsync(Expression<Func<PersonUser, bool>> query)
        {
            try
            {
                return Data.Where(query).AsQueryable();
            }
            catch (PersonUserException ex)
            {
                _logger.LogCritical(ex.Message, "PersonUserQuery >> GetAllAsync");
                throw new PersonUserException(ex.Message);
            }

        }

        /// <inheritdoc/>
        public async Task<PersonUser> GetAsync(int id)
        {
            try
            {
                return await FirstOrDefaultAsync(f => f.Id == id);
            }
            catch (PersonUserException ex)
            {
                _logger.LogCritical(ex.Message, "PersonUserQuery >> GetByIdAsync");
                throw new PersonUserException(ex.Message);
            }

        }
    }
}
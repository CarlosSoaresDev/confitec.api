﻿using Confitec.Domain.PersonModule.PersonUserFeature;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.Diagnostics.CodeAnalysis;

namespace Confitec.Data.PersonModule.PersonUserFeature
{
    /// <summary> Represents the data table mapping "person user". </summary>
    [ExcludeFromCodeCoverage]
    public class PersonUserMapping : IEntityTypeConfiguration<PersonUser>
    {
        /// <summary> Execute the configuration mapping person user. </summary>
        /// <param name="builder">Current builder entity type.</param>
        public void Configure(EntityTypeBuilder<PersonUser> builder)
        {
            builder.Property(e => e.Email).HasColumnName("Email").HasMaxLength(255).IsUnicode(false);
            builder.Property(e => e.Password).HasColumnName("Password").HasMaxLength(255);
        }
    }
}

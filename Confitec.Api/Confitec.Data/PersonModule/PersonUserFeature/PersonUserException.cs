﻿using System;

namespace Confitec.Data.PersonModule.PersonUserFeature
{
    public class PersonUserException: Exception
    {
        /// <inheritdoc />
        /// <summary>Initializes a new instance of the <see cref="PersonUserException"></see></summary>
        public PersonUserException() { }

        /// <inheritdoc />
        /// <summary>Initializes a new instance of the <see cref="PersonUserException"></see></summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        public PersonUserException(string message) : base(message) { }
    }
}

﻿namespace Confitec.Domain.SuportFeature
{
    public class FilterBase<T>
    {
        public string Search { get; set; }
        public int? Skip { get; set; }
        public int? Limit { get; set; }
        public OrderByBase OrderBy { get; set; }
        public T Filter { get; set; }
    }

    public class OrderByBase
    {
        public string Order { get; set; }
        public string Column { get; set; }
    }
}

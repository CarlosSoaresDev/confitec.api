﻿using System.Collections.Generic;

namespace Confitec.Domain.SuportFeature
{
    /// <summary>
    /// Paginated result of records
    /// </summary>
    /// <typeparam name="T">Type of result content</typeparam>
    public class PaginatedContent<T>
    {
        /// <summary>
        /// Current page number
        /// </summary>
        public int Page { get; set; }
     
        /// <summary>
        /// Records count of query
        /// </summary>
        public int Total { get; set; }
      
        /// <summary>
        /// Partial set of records to retrieve from query
        /// </summary>
        public IEnumerable<T> Content { get; set; }
    }
}
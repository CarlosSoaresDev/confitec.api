﻿using System.Linq;

namespace Confitec.Domain.Adapter
{
    /// <summary> Represents a contract of systems Auto Mapper. </summary>
    public interface IAutoMapperObjectAdapter
    {
        /// <summary>
        /// convert the received object to target object 
        /// </summary>
        /// <typeparam name="TFrom">Object generic origin</typeparam>
        /// <typeparam name="TTo">Object generic destination</typeparam>
        /// <param name="source">source object</param>
        /// <returns>the converted object</returns>
        public TTo Convert<TFrom, TTo>(TFrom source) where TFrom : class;

        /// <summary>
        /// convert the source data object to data target object 
        /// </summary>
        /// <typeparam name="TFrom">Object generic origin</typeparam>
        /// <typeparam name="TTo">Object generic destination</typeparam>
        /// <param name="source">source object</param>
        /// <returns>the converted object</returns>
        public TTo ConvertToTarget<TFrom, TTo>(TFrom source, TTo target) where TFrom : class;

        /// <summary>
        /// projects the received generic object
        /// </summary>
        /// <typeparam name="T">Generic object</typeparam>
        /// <param name="source">current object queriable</param>
        /// <returns>the converted object</returns>
        public IQueryable<T> ProjectTo<T>(IQueryable source);
    }
}

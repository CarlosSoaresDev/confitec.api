﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Confitec.Domain
{
    public interface IRepository<TEntity> : IDisposable where TEntity : EntityBase, new()
    {
        /// <summary>Provides functionality to evaluate queries against a specific data source wherein the type of the data is known. </summary>
        IQueryable<TEntity> Data { get; }

        /// <summary>
        ///     Returns the first element of a sequence that satisfies a specified condition or a default value if no such element is found.
        /// </summary>
        /// <param name="expression">A lambda expression representing the filter to be included (<c>t =&gt; t.Property==value</c>).</param>
        /// <returns>Returns the first element of a sequence, or a default value if no element is found.</returns>
        TEntity FirstOrDefault(Expression<Func<TEntity, bool>> expression);

        /// <summary>
        ///     Returns the first element of a sequence that satisfies a specified condition or a default value if no such element is found.
        /// </summary>
        /// <param name="expression">A lambda expression representing the filter to be included (<c>t =&gt; t.Property==value</c>).</param>
        /// <param name="includes">A lambda expression representing the navigation property to be included (<c>t =&gt; t.Property</c>).</param>
        /// <returns>A task that represents the asynchronous Add operation. The task result contains the first element of a sequence, or a default value if no element is found.</returns>
        Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> expression, params Expression<Func<TEntity, object>>[] includes);

        /// <summary>
        /// Saves an entity to the repository.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task SaveAsync(TEntity entity);

        /// <summary>
        /// Saves an entity to the repository.
        /// </summary>
        /// <param name="entity">Save async a list of entity on repository.</param>
        Task SaveAsync(IEnumerable<TEntity> entity);

        /// <summary>
        /// Saves an entity to the repository.
        /// </summary>
        /// <param name="entity">Save a list of entity on repository.</param>
        void Save(TEntity entity);

        /// <summary>
        /// Saves an entity to the repository.
        /// </summary>
        /// <param name="entity">Save a list of entity on repository.</param>
        void Save(IEnumerable<TEntity> entity);

        /// <summary>Saves an entity to the repository.</summary>
        /// <param name="entity">Target entity to save on repository.</param>
        void Remove(TEntity entity);

        /// <summary>Saves an entity to the repository.</summary>
        /// <param name="entity">Target entity to save on repository.</param>
        Task RemoveAsync(TEntity entity);

        /// <summary>Removes a list of entity of the repository.</summary>
        /// <param name="entities">Remove a list of entity on repository.</param>
        void Remove(IEnumerable<TEntity> entities);

        /// <summary>Removes a list of entity of the repository.</summary>
        /// <param name="entities">Remove a list of entity on repository.</param>
        void RemoveAsync(IEnumerable<TEntity> entities);
    }
}

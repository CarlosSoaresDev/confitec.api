﻿using System.Diagnostics.CodeAnalysis;

namespace Confitec.Domain
{
    [ExcludeFromCodeCoverage]
    public abstract class EntityBase
    {
        /// <summary>Current id entity.</summary>
        public int Id { get; set; }

        /// <summary> Convert the current id entity to string in new format. </summary>
        /// <returns></returns>

        public override string ToString()
        {
            return $"{GetType()} {Id}";
        }

        /// <summary> Check if objects are equals. </summary>
        /// <returns> Returns <c>true</c> if objects are equals, otherwise <c>false</c>.</returns>

        public override bool Equals(object obj)
        {
            if (!(obj is EntityBase otherEntity)) return false;
            return Id == otherEntity.Id;
        }

        /// <summary> Get the hash code of entity. </summary>
        /// <returns> Return the hash code of entity.</returns>
        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }
    }
}
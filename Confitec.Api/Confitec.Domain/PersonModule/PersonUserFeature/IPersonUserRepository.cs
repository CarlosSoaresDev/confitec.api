﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Confitec.Domain.PersonModule.PersonUserFeature
{
    /// <summary> Represents a contract of person user repository. </summary>
    public interface IPersonUserRepository
    {
        /// <summary>
        /// create a new person user, validating data for registration
        /// </summary>
        /// <param name="personUser">Current Entity of type Person User</param>
        Task CreateAsync(PersonUser personUser);

        /// <summary>
        /// Updates the data of the individual user, from the validation of the data
        /// </summary>
        /// <param name="personUser">Current Entity of type Person User</param>
        Task UpdateAsync(PersonUser personUser);

        /// <summary>
        /// Delete user person selected by its id
        /// </summary>
        /// <param name="id">Current person user id</param>
        /// <returns></returns>
        Task DeleteAsync(int id);

        /// <summary>
        /// Creates and returns a query template
        /// </summary>
        /// <param name="query">query template expression</param>
        /// <returns></returns>
        IQueryable<PersonUser> GetAllAsync(Expression<Func<PersonUser, bool>> query);

        /// <summary>
        /// Get the data from the user person
        /// </summary>
        /// <param name="id">Current person user id</param>
        /// <returns>Peraon user object data</returns>
        Task<PersonUser> GetAsync(int id);
    }
}

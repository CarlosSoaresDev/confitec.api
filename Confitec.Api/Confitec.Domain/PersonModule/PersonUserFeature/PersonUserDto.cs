﻿using System;

namespace Confitec.Domain.PersonModule.PersonUserFeature
{
    public class PersonUserDto
    {
        public string Name { get; set; }
        public string NickName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public DateTime BirthdayOrFoundationDate { get; set; }
    }

    public class PersonUserFilterDto
    {
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
    }
}

﻿using Confitec.Domain.PersonModule.PersonFeature;

namespace Confitec.Domain.PersonModule.PersonUserFeature
{
    public partial class PersonUser: Person
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}

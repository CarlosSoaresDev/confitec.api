﻿using Confitec.Domain.SuportFeature;
using System.Threading.Tasks;

namespace Confitec.Domain.PersonModule.PersonUserFeature
{
    /// <summary> Represents a contract of person user service. </summary>
    public interface IPersonUserService
    {
        /// <summary>
        /// Create a new person user
        /// </summary>
        /// <param name="model">data object of type person user</param>
        Task CreateAsync(PersonUserDto model);

        /// <summary>
        /// Delete a person user by selected id
        /// </summary>
        /// <param name="id">current person user id</param>
        Task DeleteByIdAsync(int id);

        /// <summary>
        /// Get All person user by filter
        /// </summary>
        /// <param name="personUserFilter">filter object</param>
        /// <returns>Paginated content list of type person user</returns>
        Task<PaginatedContent<PersonUserDto>> GetAllAsync(FilterBase<PersonUserFilterDto> personUserFilter);

        /// <summary>
        /// Get a sigle person user object selected by current id
        /// </summary>
        /// <param name="id">current person user id</param>
        /// <returns>Sigle object of type person user</returns>
        Task<PersonUserDto> GetByIdAsync(int id);

        /// <summary>
        /// Update the person user 
        /// </summary>
        /// <param name="model">data object of type person user</param>
        Task UpdateAsync(int id, PersonUserDto model);
    }
}

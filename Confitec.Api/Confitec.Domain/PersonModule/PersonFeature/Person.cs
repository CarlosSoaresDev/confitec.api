﻿using System;

namespace Confitec.Domain.PersonModule.PersonFeature
{
    public class Person: EntityBase
    {
        public string Name { get; set; }
        public string NickName { get; set; }
        public DateTime BirthdayOrFoundationDate { get; set; }
    }
}

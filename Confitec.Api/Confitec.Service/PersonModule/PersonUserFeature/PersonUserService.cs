﻿using Confitec.Data.PersonModule.PersonUserFeature;
using Confitec.Domain.Adapter;
using Confitec.Domain.PersonModule.PersonUserFeature;
using Confitec.Domain.SuportFeature;
using LinqKit;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace Confitec.Service.PersonModule.PersonUserFeature
{
    /// <inheritdoc/>
    public class PersonUserService : IPersonUserService
    {
        private readonly ILogger<PersonUserService> _logger;
        private readonly IPersonUserRepository _personUserRepository;
        private readonly IAutoMapperObjectAdapter _objectAdapter;

        /// <summary>Initializes a new instance of the PersonUserService. </summary>
        /// <param name="logger">log</param>
        /// <param name="personUserRepository">Repository base for person user</param>
        /// <param name="objectAdapter">Auto mapper object</param>
        public PersonUserService(
            ILogger<PersonUserService> logger,
            IPersonUserRepository personUserRepository,
            IAutoMapperObjectAdapter objectAdapter)
        {
            _personUserRepository = personUserRepository;
            _objectAdapter = objectAdapter;
            _logger = logger;
        }


        /// <inheritdoc/>
        public async Task CreateAsync(PersonUserDto model)
        {
            try
            {
                var personUser = _objectAdapter.Convert<PersonUserDto, PersonUser>(model);

                await _personUserRepository.CreateAsync(personUser);
            }
            catch (PersonUserException ex)
            {
                _logger.LogCritical(ex.Message, "PersonUserService >> CreateAsync");
                throw new PersonUserException(ex.Message);
            }
        }

        /// <inheritdoc/>
        public async Task DeleteByIdAsync(int id)
        {
            try
            {
                await _personUserRepository.DeleteAsync(id);
            }
            catch (PersonUserException ex)
            {
                _logger.LogCritical(ex.Message, "PersonUserService >> DeleteByIdAsync");
                throw new PersonUserException(ex.Message);
            }
        }

        /// <inheritdoc/>
        public async Task<PaginatedContent<PersonUserDto>> GetAllAsync(FilterBase<PersonUserFilterDto> personUserFilter)
        {
            try
            {
                var query = PredicateBuilder.True<PersonUser>();

                if (!string.IsNullOrEmpty(personUserFilter?.Search))
                    query = query.And(p => p.Name.Contains(personUserFilter.Search))
                        .Or(p => p.NickName.Contains(personUserFilter.Search))
                        .Or(p => p.Email.Contains(personUserFilter.Search));

                var queryBase = _personUserRepository.GetAllAsync(query);
                var personUsers = _objectAdapter.ProjectTo<PersonUserDto>(queryBase);

                if (string.IsNullOrEmpty(personUserFilter?.OrderBy?.Column))
                    personUsers = personUsers.OrderBy(p => p.Name).AsQueryable();
                else
                    personUsers = personUsers.OrderBy($"{personUserFilter.OrderBy.Column} {personUserFilter.OrderBy.Order}").AsQueryable();

                var total = await personUsers.CountAsync();

                if (personUserFilter.Skip.HasValue) personUsers = personUsers.Skip(personUserFilter.Skip.Value);
                if (personUserFilter.Limit.HasValue) personUsers = personUsers.Take(personUserFilter.Limit.Value);

                return new PaginatedContent<PersonUserDto>
                {
                    Total = total,
                    Content = await personUsers.ToListAsync()
                };
            }
            catch (PersonUserException ex)
            {
                _logger.LogCritical(ex.Message, "PersonUserService >> GetAllAsync");
                throw new PersonUserException(ex.Message);
            }
        }

        /// <inheritdoc/>
        public async Task<PersonUserDto> GetByIdAsync(int id)
        {
            try
            {
                var PersonUser = await _personUserRepository.GetAsync(id);

                return _objectAdapter.Convert<PersonUser, PersonUserDto>(PersonUser);
            }
            catch (PersonUserException ex)
            {
                _logger.LogCritical(ex.Message, "PersonUserService >> GetByIdAsync");
                throw new PersonUserException(ex.Message);
            }
        }

        /// <inheritdoc/>
        public async Task UpdateAsync(int id, PersonUserDto model)
        {
            try
            {
                var query = await _personUserRepository.GetAsync(id);

                var personUser = _objectAdapter.ConvertToTarget(model, query);

                await _personUserRepository.UpdateAsync(personUser);
            }
            catch (PersonUserException ex)
            {
                _logger.LogCritical(ex.Message, "PersonUserService >> UpdateAsync");
                throw new PersonUserException(ex.Message);
            }
        }
    }
}


﻿using Confitec.Domain.PersonModule.PersonUserFeature;
using FluentValidation;
using System;
using System.Diagnostics.CodeAnalysis;

namespace Confitec.Presentation.PersonModule.PersonUserFeature
{
    /// <summary>
    /// Represent class base for validation for PersonUserValidation
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class PersonUserValidation : AbstractValidator<PersonUserDto>
    {
        public PersonUserValidation()
        {
            RuleFor(x => x.Name)
                .NotEmpty().WithMessage("The name field is required.")
                .Length(3, 255).WithMessage("The name field must be between 3 to 255 characters.");
            RuleFor(x => x.Email)
                .NotEmpty().WithMessage("Email not entered")
                .EmailAddress().WithMessage("Invalid email");
            RuleFor(x => x.BirthdayOrFoundationDate)
                .NotEmpty().WithMessage("The date of birth field is required.")
                .Must(BirthDateSmallerThanToday).WithMessage("The date of birth cannot be greater than today");
        }

        /// <summary>
        /// Validate if BirthDate Smaller than today
        /// </summary>
        /// <param name="birthday">date for validation</param>
        /// <returns>boolean value</returns>
        private static bool BirthDateSmallerThanToday(DateTime birthday) =>
             birthday <= DateTime.Now;
    }
}

﻿using Confitec.Data.PersonModule.PersonUserFeature;
using Confitec.Domain.PersonModule.PersonUserFeature;
using Confitec.Domain.SuportFeature;
using Confitec.Presentation.PersonModule.PersonUserFeature;
using Confitec.Presentation.WebApi.Helper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Confitec.Presentation.WebApi.Controllers.PersonModule.PersonUserFeature
{
    [ApiController]
    [Route("api/[controller]")]
    public class PersonUserController : Controller
    {
        private readonly IPersonUserService _personUserService;

        /// <summary>Initializes a new instance of the <see cref="PersonUserController"></see></summary>
        /// <param name="personUserService"></param>
        public PersonUserController(IPersonUserService personUserService) => _personUserService = personUserService;

        /// <summary>
        /// Get All person user by filter query
        /// </summary>
        /// <param name="query">query params string for json convert</param>
        /// <returns>Paginated witch list person user</returns>
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(PaginatedContent<PersonUserDto>))]
        [HttpGet]
        public async Task<IActionResult> GetAllPersonUserByFilter(string query)
        {
            try
            {
                var personUserFilter = new FilterBase<PersonUserFilterDto>();

                if (!string.IsNullOrEmpty(query))
                    personUserFilter = query.QueryToObject<FilterBase<PersonUserFilterDto>>();

                return Ok(await _personUserService.GetAllAsync(personUserFilter));
            }
            catch (AggregateException ex) when (ex.InnerException != null && ex.InnerException is Exception)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Get a single person user data by id select
        /// </summary>
        /// <param name="id">Current person user id</param>
        /// <returns>Person user data object</returns>
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(PersonUserDto))]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPersonUserByID([FromRoute] int id)
        {
            try
            {
                if (id <= 0)
                    return BadRequest("Id must be greater than or equal to zero");

                var personUser = await _personUserService.GetByIdAsync(id);

                if (personUser == null)
                    return NotFound("Person User not founded");

                return Ok(personUser);
            }
            catch (AggregateException ex) when (ex.InnerException != null && ex.InnerException is PersonUserException)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Update current person user
        /// </summary>
        /// <param name="id">current person user id</param>
        /// <param name="personUser">data object of type person user</param>
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(void))]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPersonUser([FromRoute] int id, [FromBody] PersonUserDto personUser)
        {
            try
            {
                var validator = new PersonUserValidation().Validate(personUser);
                
                if (!validator.IsValid)
                    return BadRequest(validator.Errors);

                if (id <= 0)
                    return BadRequest("Id must be greater than or equal to zero");

                await _personUserService.UpdateAsync(id, personUser);

                return NoContent();
            }
            catch (AggregateException ex) when (ex.InnerException != null && ex.InnerException is PersonUserException)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Create a new person user
        /// </summary>
        /// <param name="personUser">object of type person user</param>
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(void))]
        [HttpPost]
        public async Task<ActionResult> PostPersonUser([FromBody] PersonUserDto personUser)
        {
            try
            {
                var validator = new PersonUserValidation().Validate(personUser);
                if (!validator.IsValid)
                    return BadRequest(validator.Errors);

                await _personUserService.CreateAsync(personUser);

                return NoContent();
            }
            catch (AggregateException ex) when (ex.InnerException != null && ex.InnerException is PersonUserException)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Delete person user by selected id
        /// </summary>
        /// <param name="id">Current person user id</param>
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(void))]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePersonUser([FromRoute] int id)
        {
            try
            {
                if (id <= 0)
                    return BadRequest();

                await _personUserService.DeleteByIdAsync(id);

                return NoContent();
            }
            catch (AggregateException ex) when (ex.InnerException != null && ex.InnerException is PersonUserException)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}

﻿using Newtonsoft.Json;

namespace Confitec.Presentation.WebApi.Helper
{
    public static class HelperExtensions
    {
        /// <summary>
        /// Convert query string in data object
        /// </summary>
        /// <typeparam name="T">Generic Object</typeparam>
        /// <param name="query">json query to convert</param>
        /// <returns>Object Convert</returns>
        public static T QueryToObject<T>(this string query) where T : class
        {
            return JsonConvert.DeserializeObject<T>(query);
        }
    }
}

﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System.Diagnostics.CodeAnalysis;

namespace Confitec.Presentation.WebApi.AppStart
{

    /// <summary> Represents a SwaggerExtensions. </summary>
    [ExcludeFromCodeCoverage]
    public static class SwaggerExtensions
    {
        /// <summary> Include in services all swaggers available of system. </summary>
        /// <param name="services">Service collection param. </param>
        public static void AddSwagger(this IServiceCollection services)
        {
            #region Swagger
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "Confitec API",
                    Description = "Backend Methods: GET - POST - PUT - DELETE"

                });

                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer",
                    BearerFormat = "JWT",
                    In = ParameterLocation.Header,
                    Description = "JWT Authorization header using the Bearer scheme."
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                    {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            }
                        },
                        new string[] {}
                    }
                    });
            });

            #endregion
        }
    }
}
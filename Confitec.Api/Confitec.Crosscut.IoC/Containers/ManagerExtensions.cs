﻿using Confitec.Data;
using Confitec.Data.Adapter;
using Confitec.Domain.Adapter;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics.CodeAnalysis;

namespace Confitec.Crosscut.IoC.Containers
{
    /// <summary> Represents a ManagerExtensions. </summary>
    [ExcludeFromCodeCoverage]
    public static class ManagerExtensions
    {
        /// <summary> Include in services all managers available of system. </summary>
        /// <param name="services">Service collection param. </param>
        public static void AddManagers(this IServiceCollection services)
        {
            services.AddScoped<IContext, ConfitecContext>();
            services.AddScoped<IAutoMapperObjectAdapter, AutoMapperObjectAdapter>();
        }
    }
}

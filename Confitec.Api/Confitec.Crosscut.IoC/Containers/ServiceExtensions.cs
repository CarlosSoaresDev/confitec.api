﻿using Confitec.Domain.PersonModule.PersonUserFeature;
using Confitec.Service.PersonModule.PersonUserFeature;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics.CodeAnalysis;

namespace Confitec.Crosscut.IoC.Containers
{
    /// <summary> Represents a ServiceExtensions. </summary>
    [ExcludeFromCodeCoverage]
    public static class ServiceExtensions
    {
        /// <summary> Include in services all services available of system. </summary>
        /// <param name="services">Service collection param. </param>
        public static void AddServices(this IServiceCollection services)
        {
            services.AddScoped<IPersonUserService, PersonUserService>();
        }
    }
}

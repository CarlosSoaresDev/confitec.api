﻿using Confitec.Data.PersonModule.PersonUserFeature;
using Confitec.Domain.PersonModule.PersonUserFeature;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics.CodeAnalysis;

namespace Confitec.Crosscut.IoC.Containers
{
    /// <summary> Represents a RepositoryExtensions. </summary>
    [ExcludeFromCodeCoverage]
    public static class RepositoryExtensions
    {
        /// <summary> Include in services all repositories available of system. </summary>
        /// <param name="services">Service collection param. </param>
        public static void AddRepositories(this IServiceCollection services)
        {
            services.AddScoped<IPersonUserRepository, PersonUserRepository>();
        }
    }
}